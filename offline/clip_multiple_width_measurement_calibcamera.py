import cv2
import matplotlib.pyplot as plt
import numpy as np
import os
import time
from datetime import datetime, timedelta
import csv
import pandas as pd
import threading


class repeated_timer(object):
    def __init__(self, interval, function, *args, **kwargs):
        self._timer = None
        self.interval = interval
        self.function = function
        self.args = args
        self.kwargs = kwargs
        self.is_running = False
        self.next_call = time.time()
        # self.start()
    def _run(self):
        self.is_running = False
        self.start()
        self.function(*self.args, **self.kwargs)
    def start(self):
        if not self.is_running:
            self.next_call += self.interval
            self._timer = threading.Timer(self.next_call - time.time(), self._run)
            self._timer.start()
            self.is_running = True
    def stop(self):
        self._timer.cancel()
        self.is_running = False


def current_time():
    now = datetime.now()
    time = now.strftime("%d/%m/%Y %H:%M:%S")
    return time


def create_real_timestamp(real_time_list, filament_width_list, time, video_fps):
    global start_date
    for second in range(filament_width_list):
        time_increase = time + timedelta(seconds=second/video_fps)
        real_time_list.append(str(time_increase))
    start_date = time_increase + timedelta(seconds=1/video_fps)
    return real_time_list


def generate_treefolder(report_name, result_folder, total_filaments):
    if not os.path.exists(result_folder):
        os.mkdir(result_folder)
    result_file = result_folder + f"\\result_{report_name}.csv"
    report_file = result_folder + f"\\result_{report_name}.txt"
    if not os.path.exists(result_file):
        with open(result_file, 'w', newline='') as result:
            field_number = ""
            field_name = ['ID', 'Timestamp']
            for index in range(total_filaments):
                field_number = "Width_" + str(index+1) + " (pixels)"
                field_name.append(field_number)
            writer = csv.DictWriter(result, fieldnames=field_name)
            writer.writeheader()
    return result_file, report_file


def generate_report(result_file, report_file, total_filaments, key_widths, width_1, time_list):
    with open(report_file, 'w') as report:
        report.write('******** Filament Width Measurement Report ********')
        report.write(f'\n\nTotal {width_1} files were measured')
        id = 0
        with open(result_file, 'a', newline='') as result:
            field_number = ""
            field_name = ['ID', 'Timestamp']
            for index in range(total_filaments):
                field_number = "Width_" + str(index+1) + " (pixels)"
                field_name.append(field_number)
            writer = csv.DictWriter(result, fieldnames=field_name)
            index_1 = 0                

            while index_1 < width_1:     #loop qua 198 width
                dictionary = {}
                keys = field_name
                timestamp = time_list[index_1]
            
                values = [index_1, timestamp]
                for number_of_width in range(total_filaments):
                    values.append(list(key_widths.values())[number_of_width][index_1])

                dictionary = dict(zip(keys, values))
                writer.writerow(dictionary)
                index_1 += 1
                
        report.write(f'\nTotal: {width_1} image(s)')
        time = current_time()
        report.write(f'\n\nExecution date: {time}')
    

def plot_line_graph(csv_path):
    plt.style.use('seaborn')
    data = pd.read_csv(csv_path)
    Id = data['ID']
    width = data['Width_1 (pixels)']
    lines = len(data)
    print("Number of line csv",lines)
    fig, ax1= plt.subplots(nrows=1, ncols=1)
    ax1.plot(Id, width, color='green', label='Width')
    ax1.legend()
    ax1.set_title('Filament measurement')
    ax1.set_xlabel('miliseconds')
    ax1.set_ylabel('Width of Filament (pixels)')
    plt.tight_layout()
    plt.show()
    return 


def drawRectangle(action, x, y, flags, *userdata):
    # Referencing global variables 
    global top_left_corner, bottom_right_corner, is_captured, top_left_x, top_left_y, bottom_right_x, bottom_right_y
    # Mark the top left corner when left mouse button is pressed
    if action == cv2.EVENT_LBUTTONDOWN:
        top_left_corner = [(x,y)]
    # When left mouse button is released, mark bottom right corner
    elif action == cv2.EVENT_LBUTTONUP:
            bottom_right_corner = [(x,y)]    
            if bottom_right_corner != top_left_corner and top_left_corner[0][0]<bottom_right_corner[0][0] and top_left_corner[0][1]<bottom_right_corner[0][1]:
                print('Last ROI catch has top left: {}, bottom right: {}'.format(top_left_corner, bottom_right_corner))
                top_left_x = top_left_corner[0][0]
                top_left_y = top_left_corner[0][1]
                bottom_right_x = bottom_right_corner[0][0]
                bottom_right_y = bottom_right_corner[0][1]
                cv2.setMouseCallback("Filament Measurement", lambda *args : None)
                is_captured = 1
            else:
                is_captured = 5


def export_every_t():
    global export_flag
    if export_flag == 1:
        time.sleep(2)
        export_flag = 0
    try:
        result_file, report_file = generate_treefolder(report_name, result_folder, total_filaments)
        time_list = create_real_timestamp(real_time_list, width_1, start_date, video_fps)
        generate_report(result_file, report_file, total_filaments, key_widths, width_1, time_list)
        print('CSV exported!!!')
    except:
        print("Can't export CSV !")
        pass
    key_widths.clear()
    real_time_list.clear()
    time_list.clear()
    export_flag = 0


def non_max_suppression(boxes, overlapThresh):
    if len(boxes)==0:
        return []
    if boxes.dtype.kind == "i":
        boxes = boxes.astype("float")

    pick = []

    x1 = boxes[:,0]
    y1 = boxes[:,1]
    x2 = boxes[:,2]
    y2 = boxes[:,3]
    area = (x2 - x1 + 1) * (y2 - y1 + 1)
    idxs = np.argsort(y2)
    while len(idxs) > 0:
        last = len(idxs) - 1
        i = idxs[last]
        pick.append(i)
        xx1 = np.maximum(x1[i], x1[idxs[:last]])  
        yy1 = np.maximum(y1[i], y1[idxs[:last]])
        xx2 = np.minimum(x2[i], x2[idxs[:last]])
        yy2 = np.minimum(y2[i], y2[idxs[:last]])
        w = np.maximum(0, xx2 - xx1 + 1)
        h = np.maximum(0, yy2 - yy1 + 1)
        overlap = (w * h) / area[idxs[:last]]
        idxs = np.delete(idxs, np.concatenate(([last],
            np.where(overlap > overlapThresh)[0])))
    return boxes[pick].astype("int")



if __name__ == '__main__':
    start_date = datetime(2021, 11, 25, 8, 55,29)
    real_time_list = []
    current_path = os.getcwd()
    result_folder = current_path + "\\" + "result_11_27_2021"
    report_name = "result_11_27_2021"

    dilation_level = 1
    kernal = np.ones((3,3),np.uint8)
    width_1 = 0
    key_widths = {}
    top_left_corner=[]
    bottom_right_corner=[]
    is_captured = 0
    
    #Input video
    # video_capture = cv2.VideoCapture('http://ACCAdmin:AquaCam@192.168.10.200:80/axis-cgi/mjpg/video.cgi?fps=30')
    video_capture = cv2.VideoCapture('20220526_10_17_to_10_20_Aura2.mp4')
    video_fps = 25
    
    timer_1_flag = 0
    export_flag = 1
    export_timer_1 = repeated_timer(5, export_every_t)

    #Camera calibration parameters
    mtx = np.array([[1.02842768e+03, 0.00000000e+00, 4.99331474e+02],
 [0.00000000e+00, 1.03072324e+03, 9.72012370e+02],
 [0.00000000e+00, 0.00000000e+00, 1.00000000e+00]])
    dist = np.array([-3.57080946e-01,  1.63559716e-01, -3.96642508e-05,  5.98538828e-04,
  -2.97518561e-02])

    
    try:
        while(video_capture.isOpened()):
        
            ret, image = video_capture.read()

            h,  w = image.shape[:2]
            newcameramtx, roi=cv2.getOptimalNewCameraMatrix(mtx, dist, (w,h), 1, (w,h))
            # undistort
            image = cv2.undistort(image, mtx, dist, None, newcameramtx)




            if is_captured == 1:
                try:
                    image_trim = image[top_left_y:bottom_right_y,top_left_x:bottom_right_x]     
                except:
                    export_timer_1.stop()
                    print("No Input video found")
                    try:
                        result_file, report_file = generate_treefolder(report_name, result_folder, total_filaments)
                        time_list = create_real_timestamp(real_time_list, width_1, start_date, video_fps)
                        generate_report(result_file, report_file, total_filaments, key_widths, width_1, time_list)
                    except:
                        pass
                    break

                #Imgage Processing 
                filament_gray = cv2.cvtColor(image_trim, cv2.COLOR_RGB2GRAY)
                filament_origin_trim_inv = cv2.adaptiveThreshold(filament_gray, 255,cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY_INV, 21, 15)
                filament_binary_inv_trim_dilation = cv2.dilate(filament_origin_trim_inv,kernal, iterations=dilation_level)
                cnts, hierarchy = cv2.findContours(filament_binary_inv_trim_dilation, cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)

                area = [cv2.contourArea(cnt) for cnt in cnts]
                area_sort = np.argsort(area)[::-1]
                bounding_boxes = []
                filament_width_list = []
                filament_width_list_pick = []
                total_filaments = 0

                for index in area_sort:
                    cnt = cnts[index]
                    x, y, w, h = cv2.boundingRect(cnt)
                    x1, y1, x2, y2 = x, y, x+w, y+h
                    bounding_boxes.append((x1,y1,x2,y2,w,h,area[index]))
                
                bounding_boxes = [box for box in bounding_boxes if box[:2]!= (0,0)]
                bounding_boxes = np.array(bounding_boxes)
                filament_width_list = non_max_suppression(bounding_boxes, 0.2)

                x_coordinate_order = []

                for (x1,y1,x2,y2,w,h,area) in filament_width_list:
                    filament_width_list_pick.append((x1,y1,x2,y2,w,h,area))
                    x_coordinate_order.append((x1))
                x_coordinate_order = list(np.argsort(x_coordinate_order))
                print("x_coordinate_order", x_coordinate_order)
                print("filament_width_list_pick",filament_width_list_pick)

                if len(filament_width_list_pick) > 0:
                    index_x_coordinate = 0
                    while index_x_coordinate < len(x_coordinate_order):
                        area_each = filament_width_list_pick[index_x_coordinate][6]
                        if area_each < 40:
                            del filament_width_list_pick[index_x_coordinate]
                            del x_coordinate_order[index_x_coordinate]
                            x_coordinate_order = list(np.argsort(x_coordinate_order))
                        index_x_coordinate += 1
                print("x_coordinate_order_after_sort", x_coordinate_order)
                print("filament_width_list_pick_after_sort",filament_width_list_pick)

                total_filaments = len(filament_width_list_pick)
                width_order = []
                try:
                    width_order = [filament_width_list_pick[index_x][4] for index_x in x_coordinate_order]
                except:
                    if export_timer_1.is_running == True:
                        export_timer_1.stop()
                    print("Width_order fault")
                    break
                print("width_order", width_order)
                
                # Tạo ra dictionary chứa key là tên width, value là list chứa kích thước
                field_number = ""
                field_name = []
                for index in range(total_filaments):
                    field_number = "width " + str(index+1)
                    field_name.append(field_number)
                                
                for width_index in range(total_filaments):        
                    if not field_name[width_index] in key_widths:
                        key_widths.update({field_name[width_index] : []})
                    key_widths[field_name[width_index]].append(str(width_order[width_index]-(2*dilation_level)))

                # print("key_widths total values: ",len(list(key_widths.values())[0]))
                #Get total rows of width list
                try:
                    width_1 = len(list(key_widths.values())[0])
                except:
                    print("key_widths out of range")
                    pass
                
                #Visualize on frame
                for (startX, startY, endX, endY, w, h, area) in filament_width_list_pick:
                    filament_origin = cv2.rectangle(image,(top_left_x + startX, top_left_y + startY),(top_left_x +endX,top_left_y + endY),(0,255,0),2)
                cv2.imshow("Dilation", filament_binary_inv_trim_dilation)
                ROI_capture = cv2.rectangle(image,(top_left_x, top_left_y),(bottom_right_x,bottom_right_y),(255,0,0),2)
                
                if timer_1_flag ==0:
                    export_timer_1.start()
                    timer_1_flag =1
                
                if is_captured == 0:
                    break
            if is_captured == 5:
                cv2.putText(image, "Warning: Click top left first then bottom right",(int(1100),int(800)),cv2.FONT_HERSHEY_COMPLEX_SMALL, 1,(0,0,255),int(2))

            key = cv2.waitKey(1) & 0xFF
            if key == ord('c'):
                is_captured = 0
                cv2.setMouseCallback("Filament Measurement", drawRectangle)
            elif key == ord('r'):
                is_captured = 0
                cv2.setMouseCallback("Filament Measurement", lambda *args : None)
            elif key == ord('e'):
                result_file, report_file = generate_treefolder(report_name, result_folder, total_filaments)
                time_list = create_real_timestamp(real_time_list, width_1, start_date, video_fps)
                generate_report(result_file, report_file, total_filaments, key_widths, width_1, time_list)
                # plot_line_graph(result_file)
            elif key == ord('q'):
                print("Program ended by user")
                try:
                    export_timer_1.stop()
                except:
                    pass
                break
            cv2.namedWindow("Filament Measurement")
            cv2.imshow("Filament Measurement", image)
    except: 
        export_timer_1.stop()
        result_file, report_file = generate_treefolder(report_name, result_folder, total_filaments)
        time_list = create_real_timestamp(real_time_list, width_1, start_date, video_fps)
        generate_report(result_file, report_file, total_filaments, key_widths, width_1, time_list)
        print("CSV Exported !!!")
        print("Completed !!!")
        

    video_capture.release()
    print('No input video found')
    print('Last ROI catch has top left: {}, bottom right: {}'.format(top_left_corner, bottom_right_corner))
    cv2.destroyAllWindows()






